simple code to train a resnet using transfer learning
code adpted from the pytorch tutorial at https://pytorch.org/tutorials/beginner/transfer_learning_tutorial.html
the data folder contains  a very small sample from the hymenoptera_data
ideally, the number of samples per class needs to be at least 100 for training a transfer learning model

the link for the whole dataset is at https://pytorch.org/tutorials/beginner/transfer_learning_tutorial.html

