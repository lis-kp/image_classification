# -*- coding: utf-8 -*-

######################################################################
    #code for training resnet using transfer learning
    #adapted from the pytorch tutorial at https://pytorch.org/tutorials/beginner/transfer_learning_tutorial.html

    #to run the script, type:
    #python resnet_server.py -datadir dir -model resnet101 -lr 0.001 -num_epochs 30
    #e.g.: python resnet_server.py -datadir data  -lr 0.001 -num_epochs 30

######################################################################

from __future__ import print_function, division

import argparse

import torch
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
from torch.optim import lr_scheduler
import numpy as np
import torchvision
from torchvision import datasets, models, transforms
import matplotlib.pyplot as plt
import time
import os
import copy

#plt.ion()   # interactive mode

gpu = torch.cuda.is_available()
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

def train_model(model, criterion, optimizer, scheduler, model_name, num_epochs=25):
    since = time.time()

    best_model_wts = copy.deepcopy(model.state_dict())
    best_acc = 0.0

    for epoch in range(num_epochs):
        print('Epoch {}/{}'.format(epoch, num_epochs - 1))
        print('-' * 10)

        # Each epoch has a training and validation phase
        for phase in ['train', 'val']:
            if phase == 'train':
                scheduler.step()
                model.train()  # Set model to training mode
            else:
                model.eval()   # Set model to evaluate mode

            running_loss = 0.0
            running_corrects = 0

            # Iterate over data.
            for inputs, labels in dataloaders[phase]:
                inputs = inputs.to(device)
                labels = labels.to(device)

                # zero the parameter gradients
                optimizer.zero_grad()

                # forward
                # track history if only in train
                with torch.set_grad_enabled(phase == 'train'):
                    outputs = model(inputs)
                    _, preds = torch.max(outputs, 1)
                    loss = criterion(outputs, labels)

                    # backward + optimize only if in training phase
                    if phase == 'train':
                        loss.backward()
                        optimizer.step()

                # statistics
                running_loss += loss.item() * inputs.size(0)
                running_corrects += torch.sum(preds == labels.data)

            epoch_loss = running_loss / dataset_sizes[phase]
            epoch_acc = running_corrects.double() / dataset_sizes[phase]

            print('{} Loss: {:.4f} Acc: {:.4f}'.format(
                phase, epoch_loss, epoch_acc))

            # deep copy the model
            if phase == 'val' and epoch_acc > best_acc:
                best_acc = epoch_acc
                best_model_wts = copy.deepcopy(model.state_dict())

        print()

    time_elapsed = time.time() - since
    print('Training complete in {:.0f}m {:.0f}s'.format(
        time_elapsed // 60, time_elapsed % 60))
    print('Best val Acc: {:4f}'.format(best_acc))
    print("")

    # load best model weights
    model.load_state_dict(best_model_wts)
    #save model
    torch.save(model.state_dict(), model_name)
    return model


#visualize_model(model_ft)
def evaluate_model(model_ft, classes_names):
    correct = 0
    total = 0
    with torch.no_grad():
        for data in dataloaders['test']:
            images, labels = data
            outputs = model_ft(images)
            _, predicted = torch.max(outputs.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()

    print('Accuracy of the network on the test images: %d %%' % (
            100 * correct / total))

    #print("len(labels), labels",len(labels), labels)

    class_correct = list(0. for i in range(2))
    class_total = list(0. for i in range(2))

    with torch.no_grad():
        for data in dataloaders['test']:
            images, labels = data
            outputs = model_ft(images)
            _, predicted = torch.max(outputs, 1)
            c = (predicted == labels).squeeze()
            # print(len(predicted), len(labels))
            for i in range(len(labels)):
                label = labels[i]
                class_correct[label] += c[i].item()
                class_total[label] += 1

    for i in range(2):
        #print(class_correct[i], class_total[i])
        print('Accuracy of %5s : %2d %%' % (
            classes_names[i], 100 * class_correct[i] / class_total[i]))

def imshow(inp, title=None):
    """Imshow for Tensor."""
    inp = inp.numpy().transpose((1, 2, 0))
    mean = np.array([0.485, 0.456, 0.406])
    std = np.array([0.229, 0.224, 0.225])
    inp = std * inp + mean
    inp = np.clip(inp, 0, 1)
    plt.imshow(inp)
    if title is not None:
        plt.title(title)
    plt.pause(0.001)  # pause a bit so that plots are updated

if __name__ == '__main__':

    ######################################################################

    #to run the script, type:
    #python resnet_server.py -datadir dir -model resnet101 -lr 0.001 -num_epochs 30

    ######################################################################

    ap = argparse.ArgumentParser()
    ap.add_argument("-model", "--model", type=str, default="resnet50",
                    help="name of pre-trained network to use")

    ap.add_argument("-model_name", "--model_name", type=str, default="model.pkl",
                    help="name of the model to save")

    ap.add_argument("-datadir", "--datadir", type=str,
                    help="name of training and testing data directory")

    ap.add_argument("-lr", "--lr", type=float,
                    help="learning rate")

    ap.add_argument("-num_epochs", "--num_epochs", type=int,
                    help="number of epochs")

    args = vars(ap.parse_args())

    data_transforms = {
        'train': transforms.Compose([
            transforms.RandomResizedCrop(224),
            transforms.RandomHorizontalFlip(),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ]),
        'val': transforms.Compose([
            transforms.Resize(256),
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ]),

        'test': transforms.Compose([
            transforms.Resize(256),
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ]),
    }

    data_dir = args["datadir"]
    num_epochs = args["num_epochs"]
    lr = args["lr"]
    model = args["model"]
    model_name = args["model_name"]

    image_datasets = {x: datasets.ImageFolder(os.path.join(data_dir, x),
                                              data_transforms[x])
                      for x in ['train', 'val', 'test']}
    dataloaders = {x: torch.utils.data.DataLoader(image_datasets[x], batch_size=4,
                                                  shuffle=True, num_workers=4)
                   for x in ['train', 'val', 'test']}
    dataset_sizes = {x: len(image_datasets[x]) for x in ['train', 'val']}

    class_names = image_datasets['train'].classes

    #print("class_names", class_names)

    # Get a batch of training data
    inputs, classes = next(iter(dataloaders['train']))

    # Make a grid from batch
    out = torchvision.utils.make_grid(inputs)

    #imshow(out, title=[class_names[x] for x in classes])

    #print("classes", classes)

    ######################################################################
    # Finetuning the convnet
    # ----------------------
    #
    # Load a pretrained model and reset final fully connected layer.
    #

    if model == "resnet101":
        model_ft = models.resnet101(pretrained=True)
    elif model == "resnet50":
        model_ft = models.resnet50(pretrained=True)
    else:
        model_ft = models.resnet18(pretrained=True)

    num_ftrs = model_ft.fc.in_features
    model_ft.fc = nn.Linear(num_ftrs, 2)

    model_ft = model_ft.to(device)

    criterion = nn.CrossEntropyLoss()

    # All parameters are being optimized
    optimizer_ft = optim.SGD(model_ft.parameters(), lr=lr, momentum=0.9)

    # Decay LR by a factor of 0.1 every 7 epochs
    exp_lr_scheduler = lr_scheduler.StepLR(optimizer_ft, step_size=7, gamma=0.1)

    ######################################################################
    # Train and evaluate
    # ^^^^^^^^^^^^^^^^^^

    model_ft = train_model(model_ft, criterion, optimizer_ft, exp_lr_scheduler, model_name,
                           num_epochs=num_epochs)
    evaluate_model(model_ft, class_names)
    ######################################################################